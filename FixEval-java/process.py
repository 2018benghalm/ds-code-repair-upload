import os 
import pandas as pd 
from tqdm import tqdm 
import json

""" 
This is a script to format a dataset of broken code/repaired code in the TFix format 
TFix inputs/outputs are JSON datapoints following the below architecture 

{
		"instructions" : 
		[
			{
				"description" : "Insert: CoreLib LN: 2 LI: 25 GI: 58 , RelativPos: 0 , RelativPos: 0",
				"global_idx" : 58,
				"line_column" : 25,
				"line_number" : 2,
				"relativ_pos" : 0,
				"text" : "CoreLib",
				"type" : 1
			}
		],
		"linter_report" : 
		{
			"col_begin" : 18,
			"col_end" : 0,
			"evidence" : "Undefined variable.",
			"line_begin" : 219,
			"line_end" : 0,
			"message" : "Undefined variable.",
			"rule_id" : "no-undef",
			"severity" : 0
		},
		"repo" : "/data/all/data/pks5/ui5strap",
		"source_changeid" : "d2763087bc394a24c4f31e0abc61177a124299b6",
		"source_code" : "transCallback,ui5strap.options.layerTimeout);",
		"source_file" : "ttransTimeout = window.setTimeout(transCallback,\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strap.options.layerTimeout);\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t$layer\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.toggleClass(\r\n",
		"source_filename" : "www/lib/ui5strap/Layer.js",
		"target_changeid" : "ee2a97af306405327a283b441822f9eefd5a8c74",
		"target_code" : "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttransCallback,\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strapCoreLib.options.layerTimeout);\r\n\r\n",
		"target_file" : "\t\t\t\t\t\t\t\t\t\t\t\t\ttransTimeout = window\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.setTimeout(\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttransCallback,\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strapCoreLib.options.layerTimeout);\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t$layer\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.toggleClass(\r\n",
		"target_filename" : "www/lib/pks/ui5strap/core/Layer.js",
		"warning_line" : "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strap.options.layerTimeout);\r\n"
	}

By examining the TFix data prep source code, we see that :
    - The "instructions" part is metadata on git changes. It is used for data cleaning and exploration but not by the model
    - For the purposes of this dataset (the FixEval dataset), codes and files are treated the same as all our inputs are 
    independant on-line java files containing one function 
    - The linter_report data is missing in the FixEval dataset, we will be using a java linter(PMD) to generate and fill this part
"""

# We start with a filled-out dict since we will only be providing some information
prefilled_datapoint = {
		"instructions" : 
		[
			{
				"description" : "Insert: CoreLib LN: 2 LI: 25 GI: 58 , RelativPos: 0 , RelativPos: 0",
				"global_idx" : 58,
				"line_column" : 25,
				"line_number" : 2,
				"relativ_pos" : 0,
				"text" : "CoreLib",
				"type" : 1
			}
		],
		"linter_report" : 
		{
			"col_begin" : 18,
			"col_end" : 0,
			"evidence" : "Undefined variable.",
			"line_begin" : 219,
			"line_end" : 0,
			"message" : "Undefined variable.",
			"rule_id" : "no-undef",
			"severity" : 0
		},
		"repo" : "/data/all/data/pks5/ui5strap",
		"source_changeid" : "d2763087bc394a24c4f31e0abc61177a124299b6",
		"source_code" : "transCallback,ui5strap.options.layerTimeout);",
		"source_file" : "ttransTimeout = window.setTimeout(transCallback,\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strap.options.layerTimeout);\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t$layer\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.toggleClass(\r\n",
		"source_filename" : "www/lib/ui5strap/Layer.js",
		"target_changeid" : "ee2a97af306405327a283b441822f9eefd5a8c74",
		"target_code" : "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttransCallback,\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strapCoreLib.options.layerTimeout);\r\n\r\n",
		"target_file" : "\t\t\t\t\t\t\t\t\t\t\t\t\ttransTimeout = window\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.setTimeout(\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttransCallback,\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strapCoreLib.options.layerTimeout);\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t$layer\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.toggleClass(\r\n",
		"target_filename" : "www/lib/pks/ui5strap/core/Layer.js",
		"warning_line" : "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tui5strap.options.layerTimeout);\r\n"
	}

# We select the violations to keep as some are not error-inducing
with open('./Ruleset.txt') as f:
        violations_to_drop = f.readlines()

# Read data
with open("/processed/src_train.java-java.java", "r") as src_codes :
    with open("./processed/tgt_train.java-java.java", "r") as target_codes :
        # Read the files containing incorrect and target code
        sources = src_codes.readlines()
        targets = target_codes.readlines()
        # Storage 
        datapoints = []
        rules = {} # This will allow filtering on rules
        # Loop through examples : 
        for (src,tgt) in tqdm(zip(sources[::50],targets[::50])):
            # To use PMD on a code sample, it needs to be in a java file : 
            with open("temp.java", "w") as temp_java_file : 
                temp_java_file.write(src)
            output_stream = os.popen("pmd-bin-6.49.0/bin/run.sh pmd -d temp.java -f json -R rulesets/java/quickstart.xml") # Path to your PMD script and ruleset
            ret = output_stream.read()
            parsed_ret = json.loads(ret)
            # We create a new data point per violation
            # TODO only keep bug_inducing violations
            # TODO filter on severity
            if parsed_ret['files']: #If PMD manages to parse the file
                for violation in parsed_ret['files'][0]["violations"] : 
                    if violation in violations_to_drop :
                        continue # Skip this violation if it is useless
                    datapoint = prefilled_datapoint
                    datapoint["linter_report"]["col_begin"] = violation['begincolumn']
                    datapoint["linter_report"]["col_end"] = violation['endcolumn']
                    datapoint["linter_report"]["line_begin"] = violation['beginline']
                    datapoint["linter_report"]["line_end"] = violation['endline']
                    datapoint["linter_report"]["evidence"] = "foo"
                    datapoint["linter_report"]["message"] = violation['description']
                    datapoint["linter_report"]["rule_id"] = violation['rule']
                    #rules.setdefault(violation['rule'],violation['description']) # Add new rules to dict
                    datapoint["linter_report"]["severity"] = violation['priority']
                    # The FixEval dataset means we do not get file-level granularity. Another dataset would refine the following part 
                    datapoint["source_code"] = src
                    datapoint["warning_line"] = src
                    datapoint["source_file"] = src
                    datapoint["target_code"] = tgt
                    datapoint["target_file"] = tgt
                    datapoints.append(datapoint)
        # Save 
        with open('FixEvalData_test.json', 'w', encoding='utf-8') as f:
            json.dump(datapoints, f, ensure_ascii=False, indent=4)
        with open('Rules_test.json', 'w', encoding='utf-8') as f:
            json.dump(rules, f, ensure_ascii=False, indent=4)
            
            
                





    